﻿using ImageProcessing.Filters;
using ImageProcessing.IO;
using ImageProcessing.Loggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing
{
    class Program
    {
        static void Main(string[] args)
        {
            string fDir = @"E:\Biblioteki\Desktop\Photos\";
            string date = string.Format("Log {0:yyyy-MM-dd_HH-mm-ss}.txt", DateTime.Now);

            CompositeLogger logger =
                new CompositeLogger(
                                    new ConsoleLogger(),
                                    new FileLogger(fDir, date),
                                    new DebugLogger());

            IOImage myImage = new IOImage();
            myImage.LoadImage(fDir, "1.jpg");

            var filters = new Contrast(new Normalize(new ImageToFilter(myImage.myBitmap)), 2);
            filters.Start();

            myImage.SaveImage(filters.GetMyBitmap(), fDir, "r", SaveType.JPEG);
            Console.ReadKey();
        }
    }
}
