﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.IO
{
    class SaveToJPEG : SaveStrategy
    {
        public SaveToJPEG()
        {
            extension = ".jpeg";
        }
        public override void Save(string fDir, string fileName)
        {
            _myBitmap.Save(fDir + fileName + ".jpeg", ImageFormat.Jpeg);
        }
    }
}
