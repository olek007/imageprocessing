﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.IO
{
    class SaveToPNG : SaveStrategy
    {
        public SaveToPNG()
        {
            extension = ".png";
        }
        public override void Save(string fDir, string fileName)
        {
            _myBitmap.Save(fDir + fileName + ".png", ImageFormat.Png);
        }
    }
}
