﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.IO
{
    class SaveToBMP : SaveStrategy
    {
        public SaveToBMP()
        {
            extension = ".bpm";
        }
        public override void Save(string fDir, string fileName)
        {
            _myBitmap.Save(fDir + fileName + extension, ImageFormat.Bmp);
        }
    }
}
