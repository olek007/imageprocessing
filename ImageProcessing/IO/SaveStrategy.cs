﻿using ImageProcessing.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.IO
{
    abstract class SaveStrategy
    {
        protected Pixel _myBitmap;
        protected string extension;
        public void SetMyBitmap(Pixel myBitmap)
        {
            _myBitmap = myBitmap;
        }

        public string GetExtension()
        {
            return extension;
        }
        public abstract void Save(string fDir, string fileName);
    }
}
