﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using ImageProcessing.Helper;

namespace ImageProcessing.IO
{
    class IOImage
    {
        public MyBitmap myBitmap;
        public void LoadImage(string fDir, string fileName)
        {
            using (Stream BitmapStream = System.IO.File.Open(fDir + fileName, System.IO.FileMode.Open))
            {
                Image img = Image.FromStream(BitmapStream);

                myBitmap = new MyBitmap(new Pixel(img), fileName);
            }
            Console.WriteLine("Loaded file: {0} \t\t- Done", fileName);
        }

        public void SaveImage(MyBitmap bitmapToSave,string fDir, string fileName, SaveType saveType = SaveType.JPEG)
        {
            BitmapSave bitmap = new BitmapSave(bitmapToSave);

            switch (saveType)
            {
                case SaveType.JPEG:
                    bitmap.SetSaveStrategy(new SaveToJPEG());
                    break;
                case SaveType.PNG:
                    bitmap.SetSaveStrategy(new SaveToPNG());
                    break;
                case SaveType.TIFF:
                    bitmap.SetSaveStrategy(new SaveToTIFF());
                    break;
                case SaveType.BMP:
                    bitmap.SetSaveStrategy(new SaveToBMP());
                    break;
                default:
                    break;
            }

            bitmap.Save(fDir, fileName);
        }
    }
}
