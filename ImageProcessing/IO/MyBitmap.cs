﻿using ImageProcessing.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.IO
{
    class MyBitmap
    {
        public Pixel bitmap;
        public string fileName;
        public MyBitmap()
        {
            
        }
        public MyBitmap(Pixel bitmap, string fileName)
        {
            this.bitmap = bitmap;
            this.fileName = fileName;
        }
    }
}
