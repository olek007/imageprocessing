﻿using ImageProcessing.Loggers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.IO
{
    class BitmapSave
    {
        protected MyBitmap _myBitmap;
        private SaveStrategy _saveStrategy;
        public BitmapSave(MyBitmap myBitmap)
        {
            _myBitmap = myBitmap;
        }

        public void SetSaveStrategy(SaveStrategy saveStrategy)
        {
            _saveStrategy = saveStrategy;
            _saveStrategy.SetMyBitmap(_myBitmap.bitmap);
        }

        public void Save(string fDir, string fileName)
        {
            fileName = fileName.Split('.')[0];
            _saveStrategy.Save(fDir, fileName);
            Logger.LogMessage(String.Format("{0} as {1}{2} \t\t- Done", _saveStrategy.GetType().Name, fileName, _saveStrategy.GetExtension()));
        }
    }
}
