﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing.Imaging;
using System.Drawing;


namespace ImageProcessing.Helper
{
    public class Pixel
    {

        public Bitmap bitmap;
        public BitmapData bitmapData;
        private IntPtr intptr = IntPtr.Zero;

        public int Height
        {
            get { return bitmap.Height; }
        }
        public int Width
        {
            get { return bitmap.Width; }
        }

        public byte[] Pixels { get; set; }
        public int PixelFormatSize { get; set; }

        public Pixel(Image img)
        {
            this.bitmap = new Bitmap(img);
        }

        public Pixel(int Width, int Height)
        {
            this.bitmap = new Bitmap(Width, Height);
        }

        public Pixel(Bitmap bmp)
        {
            this.bitmap = bmp;
        }

        public void Save(string dir, ImageFormat imFormat)
        {
            bitmap.Save(dir, imFormat);
        }
        public Bitmap Save()
        {
            UnlockBits();
            return bitmap;
        }
        public void SetPixel(int x, int y, Color color)
        {
            int cCount = PixelFormatSize / 8;

            int i = ((y * Width) + x) * cCount;

            if (PixelFormatSize == 32)
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
                Pixels[i + 3] = color.A;
            }
            if (PixelFormatSize == 24)
            {
                Pixels[i] = color.B;
                Pixels[i + 1] = color.G;
                Pixels[i + 2] = color.R;
            }
            if (PixelFormatSize == 8)
            {
                Pixels[i] = color.B;
            }
        }

        public void LockBits()
        {
            try
            {
                int AllPixel = Height * Width;

                Rectangle rectangle = new Rectangle(0, 0, Width, Height);

                PixelFormatSize = System.Drawing.Bitmap.GetPixelFormatSize(bitmap.PixelFormat);

                bitmapData = bitmap.LockBits(rectangle, ImageLockMode.ReadWrite,
                                             bitmap.PixelFormat);

                int step = PixelFormatSize / 8;
                Pixels = new byte[AllPixel * step];
                intptr = bitmapData.Scan0;

                System.Runtime.InteropServices.Marshal.Copy(intptr, Pixels, 0, Pixels.Length);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void UnlockBits()
        {
            try
            {
                System.Runtime.InteropServices.Marshal.Copy(Pixels, 0, intptr, Pixels.Length);

                bitmap.UnlockBits(bitmapData);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public Color GetPixel(int x, int y)
        {
            Color color = Color.Empty;

            int cCount = PixelFormatSize / 8;

            int i = ((y * Width) + x) * cCount;

            try
            {
                if (i > Pixels.Length - cCount) ;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            if (PixelFormatSize == 32)
            {
                int b = Pixels[i];
                int g = Pixels[i + 1];
                int r = Pixels[i + 2];
                int a = Pixels[i + 3];
                color = Color.FromArgb(a, r, g, b);
            }
            else
            if (PixelFormatSize == 24)
            {
                int b = Pixels[i];
                int g = Pixels[i + 1];
                int r = Pixels[i + 2];
                color = Color.FromArgb(r, g, b);
            }
            else
            if (PixelFormatSize == 8)
            {
                int c = Pixels[i];
                color = Color.FromArgb(c, c, c);
            }
            return color;
        }

        public static implicit operator Pixel(Bitmap v)
        {
            throw new NotImplementedException();
        }
    }
}
