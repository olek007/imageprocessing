﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Loggers
{
    abstract class ILogger
    {
        protected readonly object lockObj = new object();
        public abstract void LogMessage(string message);
    }
}
