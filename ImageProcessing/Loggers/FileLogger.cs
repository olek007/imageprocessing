﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Loggers
{
    class FileLogger : ILogger
    {
        private string _fDir;
        private string _fileName;
        public FileLogger(string fDir, string fileName)
        {
            _fDir = fDir;
            _fileName = fileName;
        }
        public override void LogMessage(string message)
        {
            lock (lockObj)
            {
                using (StreamWriter writer = File.AppendText(_fDir + _fileName))
                {
                    
                    writer.WriteLine(message);
                }
            }
        }
    }
}
