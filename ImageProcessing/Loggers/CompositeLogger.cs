﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Loggers
{
    class CompositeLogger : ILogger
    {
        private static CompositeLogger _instance = null;
        private ILogger[] _loggers;
        public CompositeLogger(params ILogger[] loggers)
        {
            if (_instance == null)
            {
                _loggers = loggers;
                Logger.SetLogger(this);
                _instance = this;
            }
            else
            {
                Logger.LogMessage(String.Format("Cannot create more then one instance of {0} class", this.GetType().Name));
            }
        }
        public override void LogMessage(string message)
        {
            foreach (ILogger logger in _loggers)
            {
                logger.LogMessage(message);
            }
        }
    }
}
