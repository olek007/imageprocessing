﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Loggers
{
    class Logger
    {
        private static ILogger _logger;
        public static void SetLogger(ILogger logger)
        {
            _logger = logger;
        }
        public static void LogMessage(string message)
        {
            _logger.LogMessage(message);
        }
    }
}
