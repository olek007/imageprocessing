﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Loggers
{
    class DebugLogger : ILogger
    {
        public override void LogMessage(string message)
        {
            lock(lockObj)
            {
                Debug.WriteLine(message);
            }
        }
    }
}
