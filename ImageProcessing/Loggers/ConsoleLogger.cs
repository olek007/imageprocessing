﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Loggers
{
    class ConsoleLogger : ILogger
    {
        public override void LogMessage(string message)
        {
            lock (lockObj)
            {
                Console.WriteLine(message);
            }
        }
    }
}
