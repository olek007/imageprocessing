﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Loggers
{
    class EventLogger : ILogger
    {
        private string _source;
        private string _log;
        public EventLogger()
        {
            _source = "Application";
            _log = "MyLog";
        }
        public override void LogMessage(string message)
        {
            lock (lockObj)
            {
                if (!EventLog.SourceExists(_source))
                    EventLog.CreateEventSource(_source, _log);
                EventLog.WriteEntry(_source, message, EventLogEntryType.Information);
            }
        }
    }
}
