﻿using ImageProcessing.Loggers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Filters
{
    class FilterDecorator : IFilter
    {
        private IFilter _iFilter;
        public FilterDecorator(IFilter iFilter)
        {
            _iFilter = iFilter;
            SetBitmap(_iFilter.GetBitmap());
            SetFileName(_iFilter.GetFileName());

            filterDelegate = Filter;
            filterDelegate += LogEndFilter;
        }
        private void LogEndFilter()
        {
            Logger.LogMessage(String.Format("{0} : {1} \t\t- Done", GetFileName(), this.GetType().Name));
        }
        protected override void Filter()
        {
            _iFilter.filterDelegate.Invoke();

            SetBitmap(_iFilter.GetBitmap());

            Logger.LogMessage(String.Format("{0} : {1} \t\t- Start", GetFileName(), this.GetType().Name));
        }

        public void Start()
        {
            filterDelegate.Invoke();
        }

        
    }
}
