﻿using ImageProcessing.Helper;
using ImageProcessing.IO;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Filters
{
    abstract class IFilter
    {
        public delegate void FilterDelegate();
        public FilterDelegate filterDelegate;

        private MyBitmap _myBitmap = new MyBitmap();

        public void SetBitmap(Pixel newBitmap)
        {
            _myBitmap.bitmap = newBitmap;
        }

        public Pixel GetBitmap()
        {
            return _myBitmap.bitmap;
        }

        public void SetFileName(string fileName)
        {
            _myBitmap.fileName = fileName;
        }

        public string GetFileName()
        {
            return _myBitmap.fileName;
        }

        public MyBitmap GetMyBitmap()
        {
            return _myBitmap;
        }
        protected abstract void Filter();
    }
}
