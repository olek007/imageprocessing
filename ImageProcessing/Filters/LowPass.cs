﻿using ImageProcessing.Helper;
using ImageProcessing.Loggers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Filters
{
    class LowPass : FilterDecorator
    {
        private int[][] _se;

        public LowPass(IFilter iFilter, int[][] se = null) : base(iFilter)
        {
            se = se ?? FilterMask.LowPass1;
            _se = se;
        }

        protected override void Filter()
        {
            base.Filter();
            Pixel tempBitmap = GetBitmap();
            Pixel newBitmap = new Pixel(tempBitmap.bitmap.Width, tempBitmap.bitmap.Height);

            tempBitmap.LockBits();
            newBitmap.LockBits();

            for (int x = 0; x < tempBitmap.Width; x++)
            {
                for (int y = 0; y < tempBitmap.Height; y++)
                {
                    int sumA = 0;
                    int sumR = 0;
                    int sumG = 0;
                    int sumB = 0;

                    int seSize = _se.Length;

                    for (int i = 0; i < seSize; i++)
                    {
                        for (int j = 0; j < seSize; j++)
                        {
                            int xCor = x + (i - 1);
                            int yCor = y + (j - 1);

                            if (xCor < 0)
                            {
                                xCor = 0;
                            }
                            else
                            if (xCor >= tempBitmap.Width - 1)
                            {
                                xCor = tempBitmap.Width - 1;
                            }

                            if (yCor < 0)
                            {
                                yCor = 0;
                            }
                            else
                            if (yCor >= tempBitmap.Height - 1)
                            {
                                yCor = tempBitmap.Height - 1;
                            }

                            Color pix = tempBitmap.GetPixel(xCor, yCor);
                            sumA += _se[i][j] * pix.A;
                            sumR += _se[i][j] * pix.R;
                            sumG += _se[i][j] * pix.G;
                            sumB += _se[i][j] * pix.B;
                        }
                    }
                    int seSum = _se.Sum(r => r.Sum());
                    sumA /= seSum;
                    sumR /= seSum;
                    sumG /= seSum;
                    sumB /= seSum;

                    sumA = Math.Min(Math.Max(sumA, 0), 255);
                    sumR = Math.Min(Math.Max(sumR, 0), 255);
                    sumG = Math.Min(Math.Max(sumG, 0), 255);
                    sumB = Math.Min(Math.Max(sumB, 0), 255);

                    Color newPix = Color.FromArgb(sumA, sumR, sumG, sumB);
                    newBitmap.SetPixel(x, y, newPix);
                }
            }

            newBitmap.UnlockBits();
            tempBitmap.UnlockBits();

            SetBitmap(newBitmap);
        }
    }
}
