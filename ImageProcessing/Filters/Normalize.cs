﻿using ImageProcessing.Loggers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using ImageProcessing.Helper;

namespace ImageProcessing.Filters
{
    class Normalize : FilterDecorator
    {
        public Normalize(IFilter iFilter) : base(iFilter)
        {
        }

        protected override void Filter()
        {
            base.Filter();
            Pixel tempBitmap = GetBitmap();
            Pixel newBitmap = new Pixel(tempBitmap.bitmap.Width, tempBitmap.bitmap.Height);

            tempBitmap.LockBits();
            newBitmap.LockBits();

            int RMin = 255;
            int GMin = 255;
            int BMin = 255;

            int RMax = 0;
            int GMax = 0;
            int BMax = 0;


            for (int x = 0; x < tempBitmap.Width; x++)
            {
                for (int y = 0; y < tempBitmap.Height; y++)
                {
                    Color pix = tempBitmap.GetPixel(x, y);
                    if (pix.R < RMin)
                    {
                        RMin = pix.R;
                    }
                    else
                    if (pix.R > RMax)
                    {
                        RMax = pix.R;
                    }

                    if (pix.G < GMin)
                    {
                        GMin = pix.G;
                    }
                    else
                    if (pix.G > GMax)
                    {
                        GMax = pix.G;
                    }

                    if (pix.B < BMin)
                    {
                        BMin = pix.B;
                    }
                    else
                    if (pix.B > BMax)
                    {
                        BMax = pix.B;
                    }
                }
            }

            for (int x = 0; x < tempBitmap.Width; x++)
            {
                for (int y = 0; y < tempBitmap.Height; y++)
                {
                    Color pix = tempBitmap.GetPixel(x, y);

                    int newR = (pix.R - RMin) * (255 / (RMax - RMin));
                    int newG = (pix.G - GMin) * (255 / (GMax - GMin));
                    int newB = (pix.B - BMin) * (255 / (BMax - BMin));

                    Color newPix = Color.FromArgb(pix.A, newR, newG, newB);

                    newBitmap.SetPixel(x, y, newPix);
                }
            }

            newBitmap.UnlockBits();
            tempBitmap.UnlockBits();

            SetBitmap(newBitmap);
        }
    }
}
