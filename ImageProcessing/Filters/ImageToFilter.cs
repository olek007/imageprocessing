﻿using ImageProcessing.IO;
using ImageProcessing.Loggers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Filters
{
    class ImageToFilter : IFilter
    {
        public ImageToFilter(MyBitmap myImage)
        {
            filterDelegate = Filter;

            SetBitmap(myImage.bitmap);
            SetFileName(myImage.fileName);
        }

        protected override void Filter()
        {
            Logger.LogMessage("Start Processing Image \t\t- Done");
        }
    }
}
