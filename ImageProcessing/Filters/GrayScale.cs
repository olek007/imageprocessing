﻿using ImageProcessing.Helper;
using ImageProcessing.Loggers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Filters
{
    class GrayScale : FilterDecorator
    {
        public GrayScale(IFilter iFilter) : base(iFilter)
        {
        }

        protected override void Filter()
        {
            base.Filter();
            Pixel tempBitmap = GetBitmap();
            Pixel newBitmap = new Pixel(tempBitmap.bitmap.Width, tempBitmap.bitmap.Height);

            tempBitmap.LockBits();
            newBitmap.LockBits();

            for (int x = 0; x < tempBitmap.Width; x++)
            {
                for (int y = 0; y < tempBitmap.Height; y++)
                {
                    Color pix = tempBitmap.GetPixel(x, y);
                    int gray = (int)((pix.R * 0.299) + (pix.G * 0.587) + (pix.B * 0.114));
                    Color newPix = Color.FromArgb(pix.A, gray, gray, gray);
                    newBitmap.SetPixel(x, y, newPix);
                }
            }

            newBitmap.UnlockBits();
            tempBitmap.UnlockBits();

            SetBitmap(newBitmap);
        }
    }
}
