﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Filters
{
    class FilterMask
    {
        //Prewitt Filter Masks
        public static readonly int[][] Prewitt_0 =
        {
            new int[] {-1, 0, 1},
            new int[] {-1, 0, 1},
            new int[] {-1, 0, 1}
        };
        public static readonly int[][] Prewitt_45 =
        {
            new int[] { 0, 1, 1},
            new int[] {-1, 0, 1},
            new int[] {-1,-1, 0}
        };
        public static readonly int[][] Prewitt_90 =
        {
            new int[] { 1, 1, 1},
            new int[] { 0, 0, 0},
            new int[] {-1,-1,-1}
        };
        public static readonly int[][] Prewitt_135 =
        {
            new int[] { 1, 1, 0},
            new int[] { 1, 0,-1},
            new int[] { 0,-1,-1}
        };

        //Sobel Filter Masks
        public static readonly int[][] Sobel_0 =
        {
            new int[] {-1, 0, 1},
            new int[] {-2, 0, 2},
            new int[] {-1, 0, 1}
        };
        public static readonly int[][] Sobel_45 =
        {
            new int[] { 0, 1, 2},
            new int[] {-1, 0, 1},
            new int[] {-2,-1, 0}
        };
        public static readonly int[][] Sobel_90 =
        {
            new int[] { 1, 2, 1},
            new int[] { 0, 0, 0},
            new int[] {-1,-2,-1}
        };
        public static readonly int[][] Sobel_135 =
        {
            new int[] { 2, 1, 0},
            new int[] { 1, 0,-1},
            new int[] { 0,-1,-2}
        };

        //Laplace Filter Masks
        public static readonly int[][] Laplace_4 =
{
            new int[] { 0,-1, 0},
            new int[] {-1, 4,-1},
            new int[] { 0,-1, 0}
        };
        public static readonly int[][] Laplace_8 =
        {
            new int[] {-1,-1,-1},
            new int[] {-1, 8,-1},
            new int[] {-1,-1,-1}
        };

        //LowPass
        public static readonly int[][] LowPass1 =
{
            new int[] { 1, 1, 1},
            new int[] { 1, 1, 1},
            new int[] { 1, 1, 1}
        };
        public static readonly int[][] LowPass2 =
        {
            new int[] { 1, 1, 1},
            new int[] { 1, 2, 1},
            new int[] { 1, 1, 1}
        };
        public static readonly int[][] LowPass3 =
        {
            new int[] { 1, 1, 1},
            new int[] { 1, 4, 1},
            new int[] { 1, 1, 1}
        };
        public static readonly int[][] LowPass4 =
        {
            new int[] { 1, 2, 1},
            new int[] { 2, 4, 2},
            new int[] { 1, 2, 1}
        };
    }
}
