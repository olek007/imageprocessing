﻿using ImageProcessing.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageProcessing.Filters
{
    class Contrast : FilterDecorator
    {
        private float _contrast = 1;
        public Contrast(IFilter iFilter, float contrast = 1) : base(iFilter)
        {
            _contrast = contrast;
        }

        protected override void Filter()
        {
            base.Filter();
            Pixel tempBitmap = GetBitmap();
            Pixel newBitmap = new Pixel(tempBitmap.bitmap.Width, tempBitmap.bitmap.Height);

            tempBitmap.LockBits();
            newBitmap.LockBits();

            for (int x = 0; x < tempBitmap.Width; x++)
            {
                for (int y = 0; y < tempBitmap.Height; y++)
                {
                    Color pix = tempBitmap.GetPixel(x, y);

                    var newR = ((((pix.R / 255.0) - 0.5) * _contrast) + 0.5) * 255.0;
                    var newG = ((((pix.G / 255.0) - 0.5) * _contrast) + 0.5) * 255.0;
                    var newB = ((((pix.B / 255.0) - 0.5) * _contrast) + 0.5) * 255.0;
                    if (newR > 255) newR = 255;
                    if (newR < 0) newR = 0;
                    if (newG > 255) newG = 255;
                    if (newG < 0) newG = 0;
                    if (newB > 255) newB = 255;
                    if (newB < 0) newB = 0;

                    Color newPix = Color.FromArgb(pix.A, (int)newR, (int)newG, (int)newB);


                    newBitmap.SetPixel(x, y, newPix);
                }
            }

            newBitmap.UnlockBits();
            tempBitmap.UnlockBits();

            SetBitmap(newBitmap);
        }
    }
}
